-- MIT License
--
-- Copyright (c) 2023 Andrey Listopadov
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included in all
-- copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
-- SOFTWARE.

assert(debug, "debug library is not available")
assert(debug.getlocal, "debug.getlocal is not available")
assert(debug.getupvalue, "debug.getupvalue is not available")
assert(debug.getinfo, "debug.getinfo is not available")

local Nil = setmetatable(
    {},
    { __tostring = function () return tostring(nil) end }
)

local function locals()
    local variables, id, level = {}, 0, 3
    while true do
        id = id + 1
        local ok, name, val = pcall(debug.getlocal, level, id)
        if ok and nil ~= name then
            if val == nil then val = Nil end
            variables[name] = val
        elseif ok and name == nil then
            level = 1 + level
            id = 0
        else
            break
    end end
    return variables
end

local function upvalues()
    local variables, id, level = {}, 0, 2
    while true do
        id = 1 + id
        local info = debug.getinfo(level, "f") or {}
        local func = info.func
        if not func then break end
        local ok, name, val = pcall(debug.getupvalue, func, id)
        if ok and nil ~= name then
            if val == nil then val = Nil end
            variables[name] = val
        elseif ok and name == nil then
            level = level + 1
            id = 0
        else
            break
    end end
    return variables
end

return function (str, values)
    if nil == values then
        values = setmetatable(
            locals(),
            { __index = setmetatable(upvalues(), { __index = _G }) }
        )
    end
    return (str:gsub(
        "(.-){([%d%w_]+)}",
        function (str, var)
            return str .. tostring(values[var])
        end
    ))
end
