# String Interpolation for Lua

This library provides a string interpolation feature for Lua runtime.
It heavily relies on the `debug.getlocal` and `debug.getupvalue` functions and would refuse to load if those are not present.

## Installation

Copy [interpolation.lua](https://gitlab.com/andreyorst/interpolation.lua/-/raw/main/interpolation.lua) somewhere into your project.

## Usage

Require the library and store it as a function:

``` lua
f = require "interpolation"
```

Pass strings, containing `{variable}` patterns to the function `f`:

``` lua
function greet (name)
    print(f"Hi, {name}!")
end

greet("Mom")
```

Locals, upvalues, and globals are supported:

``` lua
x = 10 -- global x

do
   local x = 42 -- local x sadows the global x
   print(f"x = {x}") -- "x = 42"
end

print(f"x = {x}") -- "x = 10"

function make_adder(a)
   return function (b)
      print(f"{a} + {b}") -- handling closures
      return a + b
   end
end

add3 = make_adder(5)
add3(8) -- "5 + 8"
```

Additionally, the function accepts a table with names for variables to use instead of the locals:

```lua
f("x = {x}", {x = 72}) -- "x = 72"
```

Note that this prevents the locals lookup:

```lua
local y = 21
f("y = {y}", {x = 72}) -- "y = nil"
```
